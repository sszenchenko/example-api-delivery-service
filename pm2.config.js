module.exports = {
  apps: [{
    name: 'server',
    script: './src/server.js',
    exec_mode: 'cluster',
    instances: 'max',
    instance_var: 'APP_INSTANCE',
    merge_logs: true,
    env: {
      NODE_ENV: 'production',
    },
  }],
};
