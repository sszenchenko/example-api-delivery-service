const config = require('config');

module.exports = {
  client: 'pg',
  connection: config.get('db'),
  migrations: {
    directory: './src/db/migrations',
  },
  seeds: {
    directory: './src/db/seeds',
  },
};
