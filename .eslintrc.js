module.exports = {
  extends: [
    'airbnb-base',
    'plugin:jest/recommended',
  ],
  env: {
    node: true,
    es6: true,
    'jest/globals': true,
  },
  plugins: ['jest'],
  rules: {
    'no-use-before-define': ['error', { functions: false }],
    'no-plusplus': ['error', { 'allowForLoopAfterthoughts': true }],
    'camelcase': ['error', { properties: 'never' }],
    'max-len': ['error', { code: 120 }],
    'no-return-await': 0,
    'no-await-in-loop': 0,
    'no-param-reassign': 0,
    'consistent-return': 0,
    'default-case': 0,
    'no-prototype-builtins': 0,
    'prefer-template': 0,
    'class-methods-use-this': 0,
    'arrow-body-style': 0,
    'object-curly-newline': 0,
  }
};
