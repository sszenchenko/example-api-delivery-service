class Controller {
  static get model() {
    return null;
  }

  static handleAsync(fn) {
    return async (req, res, next) => {
      try {
        const data = await fn(req, res);
        res.send({ data });
      } catch (e) {
        next(e);
      }
    };
  }

  static getAll() {
    return this.handleAsync(async (req) => {
      const { page, filter, fields, sort } = req.query;
      const fetch = this.model.query().page(page.number, page.size);

      if (filter && Object.keys(filter).length > 0) {
        fetch.where(filter);
      }

      if (fields) {
        fetch.select(fields);
      }

      if (sort) {
        fetch.orderBy(sort);
      }

      return await fetch;
    });
  }

  static getById() {
    return this.handleAsync(req => req.entity);
  }

  static create() {
    return this.handleAsync((req, res) => {
      res.status(201);
      return this.model.query().insert(req.body).returning('*');
    });
  }

  static update() {
    return this.handleAsync(async (req) => {
      return this.model.query().patchAndFetchById(req.params.id, req.body);
    });
  }

  static delete() {
    return this.handleAsync(async (req) => {
      await this.model.query().deleteById(req.params.id);
    });
  }

  static checkEntityExists(userField = 'user_id') {
    return async (req, res, next) => {
      try {
        const where = { id: req.params.id };

        if (req.user.isUser) {
          where[userField] = req.user.id;
        }

        req.entity = await this.model.query().findOne(where).throwIfNotFound();
        next();
      } catch (e) {
        next(e);
      }
    };
  }
}

module.exports = Controller;
