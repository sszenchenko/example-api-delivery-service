const Controller = require('./Controller');
const db = require('../db');
const { Product, Order, OrderProduct } = require('../models');
const ApiError = require('../utils/ApiError');

class OrderController extends Controller {
  static get model() {
    return Order;
  }

  static getAll() {
    return this.handleAsync((req) => {
      const { query: { page }, user } = req;
      const fetch = Order.query().page(page.number, page.size).orderBy('created_at', 'desc');

      if (user.isUser) {
        fetch.where('user_id', user.id);
      }

      return fetch;
    });
  }

  static getById() {
    return this.handleAsync(async (req) => {
      const { user } = req;
      const fetch = Order.query().findById(req.params.id).eager('products.product');

      if (user.isUser) {
        fetch.where('user_id', user.id);
      }

      const res = await fetch;
      res.products = res.products.map(el => ({ ...el.product, price: el.price }));

      return res;
    });
  }

  static insert() {
    return this.handleAsync(req => db.transaction(async (trx) => {
      const { body: data, user } = req;
      const products = await Product.query().select('id', 'price').findByIds(data.product_ids);

      if (products.length !== data.product_ids.length) {
        const diff = data.product_ids.filter(id => !(products.some(p => id === p.id)));
        throw ApiError.badRequest(`Products with ids ${diff.join(', ')} don't exist`);
      }

      data.price = products.reduce((acc, curr) => acc + curr.price, 0);
      data.user_id = user.id;

      const order = await Order.query(trx).insert(data);

      await OrderProduct.query(trx).insert(products.map(el => ({
        order_id: order.id,
        product_id: el.id,
        price: el.price,
      })));

      return order;
    }));
  }

  static update() {
    return this.handleAsync((req) => {
      const { body: data, entity: order, user } = req;

      switch (data.status) {
        case Order.STATUS_ACCEPTED:
          if (order.status !== Order.STATUS_PENDING) {
            throw ApiError.badRequest('Order is already in progress');
          }

          data.courier_id = user.id;
          break;

        case Order.STATUS_DONE:
          if (order.courier_id !== user.id) {
            throw ApiError.notFound('Order doesn\'t exist');
          }

          if (order.status === Order.STATUS_DONE) {
            throw ApiError.badRequest('Order is already done');
          }

          break;
      }

      return Order.query().patchAndFetchById(req.params.id, data);
    });
  }
}

module.exports = OrderController;
