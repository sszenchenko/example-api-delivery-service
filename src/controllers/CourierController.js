const UserController = require('./UserController');
const { Courier } = require('../models');

class CourierController extends UserController {
  static get model() {
    return Courier;
  }
}

module.exports = CourierController;
