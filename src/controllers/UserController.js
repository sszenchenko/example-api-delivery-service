const { UniqueViolationError } = require('objection-db-errors');
const AuthController = require('./AuthController');
const ApiError = require('../utils/ApiError');
const { User } = require('../models');

const throwSaveError = (err, data) => {
  if (err instanceof UniqueViolationError) {
    const errMsg = err.columns[0] === 'login'
      ? `Пользователь с логином "${data.login}" уже существует`
      : `Пользователь с email "${data.email}" уже существует`;

    throw ApiError.conflict(errMsg);
  }

  throw err;
};

class UserController extends AuthController {
  static get model() {
    return User;
  }

  static getAll() {
    return this.handleAsync((req) => {
      const { filter: { search }, page } = req.query;
      const fetch = this.model.query().page(page.number, page.size);

      if (search) {
        fetch.whereRaw('name ilike %:search% or email ilike %:search% or login ilike %:search%', { search });
      }

      return fetch;
    });
  }

  static list() {
    return this.handleAsync(async () => {
      const res = await this.model.query().select('id', 'name').orderBy('name');
      return { results: res, total: res.length };
    });
  }

  static create() {
    return this.handleAsync(async (req) => {
      try {
        return await this.model.query().insert(req.body).returning('*');
      } catch (e) {
        throw throwSaveError(e, req.body);
      }
    });
  }

  static update() {
    return this.handleAsync(async (req) => {
      try {
        return await this.model.query().patchAndFetchById(req.params.id, req.body);
      } catch (e) {
        throw throwSaveError(e, req.body);
      }
    });
  }
}

module.exports = UserController;
