const Controller = require('./Controller');
const { Product } = require('../models/Product');

class ProductController extends Controller {
  static get model() {
    return Product;
  }

  static getAll() {
    return this.handleAsync((req) => {
      const { filter, page } = req.query;
      const fetch = Product.query().page(page.number, page.size);

      if (filter.name) {
        fetch.whereRaw('name ilike %:name%', filter);
      }

      if (filter.min_price) {
        fetch.where('price', '>=', filter.min_price);
      }

      if (filter.max_price) {
        fetch.where('price', '<=', filter.max_price);
      }

      return fetch;
    });
  }
}

module.exports = ProductController;
