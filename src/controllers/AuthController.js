const bcrypt = require('bcryptjs');
const moment = require('moment');
const Controller = require('./Controller');
const ApiError = require('../utils/ApiError');
const { auth } = require('../middlewares');

const setSession = async (res, user) => {
  const token = await auth.signToken(user);

  res.cookie('sid', token, {
    httpOnly: true,
    expires: moment().add(7, 'd').toDate(),
    secure: process.env.NODE_ENV === 'production',
  });

  return { token };
};

class AuthController extends Controller {
  static get model() {
    return null;
  }

  static getProfile() {
    return this.handleAsync(req => req.user);
  }

  static login() {
    return this.handleAsync(async (req, res) => {
      const data = req.body;

      const user = await this.model.query()
      // eslint-disable-next-line func-names
        .where(function () {
          this.where({ email: data.username }).orWhere({ login: data.username });
        })
        .first();

      if (!user) {
        throw ApiError.unauthorized('Пользователь с указанным Логином или Email не найден');
      }

      const passwordEqual = await bcrypt.compare(data.password, user.password);

      if (!passwordEqual) {
        throw ApiError.unauthorized('Указан неправильный пароль');
      }

      return setSession(res, user);
    });
  }

  static logout() {
    return (req, res) => {
      res.clearCookie('sid');
      res.sendStatus(200);
    };
  }
}

module.exports = AuthController;
