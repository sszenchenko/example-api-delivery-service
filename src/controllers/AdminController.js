const AuthController = require('./AuthController');
const Admin = require('../models/Admin');

class AdminController extends AuthController {
  static get model() {
    return Admin;
  }
}

module.exports = AdminController;
