require('dotenv').config();
require('moment/locale/ru');
require('./db');

const express = require('express');
const path = require('path');
const helmet = require('helmet');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const { ValidationError, NotFoundError } = require('objection');
const router = require('./routes');
const ApiError = require('./utils/ApiError');
const logger = require('./utils/logger');
const { validate } = require('./middlewares');
const { fetchSchema } = require('./utils/validator/schemas');

const app = express();

app.use(morgan(':remote-addr - :method :url :status :response-time ms', { stream: logger.stream }));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));
app.use(express.static(path.join(__dirname, '..', 'public')));
app.use(cookieParser());
app.use(helmet());
app.use(cors());

app.get('/__webpack_hmr', (req, res) => res.sendStatus(204));
app.get('/api/v1/*', validate({ query: fetchSchema }));

app.use('/', router);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(ApiError.notFound('API not found'));
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  if (!(err instanceof ApiError)) {
    if (err instanceof ValidationError) {
      err = ApiError.badRequest(err.message || 'Ошибка Валидации', err.data);
    } else if (err instanceof NotFoundError) {
      // todo remove message
      err = ApiError.notFound('Данные не найдены');
    } else {
      logger.error(err);
      err = ApiError.internal();
    }
  }

  res.format({
    'text/html': () => {
      res.sendStatus(err.statusCode);
    },
    'application/json': () => {
      res.status(err.statusCode).send(err);
    },
    default: () => {
      res.sendStatus(err.statusCode);
    },
  });
});

module.exports = app;
