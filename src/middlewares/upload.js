const ApiError = require('../utils/ApiError');

const middleware = upload => (req, res, next) => upload(req, res, (err) => {
  if (err) return next(err);

  if (Object.prototype.hasOwnProperty.call(req.body, 'json')) {
    try {
      req.body = JSON.parse(req.body.json);
    } catch (e) {
      next(ApiError.badRequest('"json" is not valid JSON string'));
    }
  }

  next();
});

module.exports = middleware;
