const ApiError = require('../utils/ApiError');

function ensureFormData(req, res, next) {
  if (!req.headers['content-type'] || !req.headers['content-type'].startsWith('multipart/form-data')) {
    res.status(415).send(ApiError.unsupportedMediaType('"multipart/form-data" is required'));
  }

  next();
}

module.exports = ensureFormData;
