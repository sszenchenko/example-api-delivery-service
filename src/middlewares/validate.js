const ApiError = require('../utils/ApiError');
const { validate } = require('../utils/validator');

function validateRequest(config, options = {}) {
  return (req, res, next) => {
    const errors = {};

    ['query', 'params', 'body', 'headers', 'files'].forEach((key) => {
      if (config.hasOwnProperty(key)) {
        const data = req[key];
        const schema = config[key];

        if (key === 'query') {
          options.stripUnknown = true;

          if (typeof data.fields === 'string') {
            data.fields = data.fields.split(',');
          }
        }

        try {
          req[key] = validate(data, schema, options);
        } catch (e) {
          errors[key] = e.data;
        }
      }
    });

    if (Object.keys(errors).length > 0) {
      res.status(400).send(ApiError.badRequest('Validation Failed', errors));
      return;
    }

    next();
  };
}

module.exports = validateRequest;
