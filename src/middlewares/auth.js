const Promise = require('bluebird');
const jwt = require('jsonwebtoken');
const config = require('config');
const ApiError = require('../utils/ApiError');
const { Admin, User } = require('../models');

const jwtSecret = config.get('jwtSecret');
const signJwt = Promise.promisify(jwt.sign);
const verifyJwt = Promise.promisify(jwt.verify);

const signToken = (user, opts) => {
  const payload = { id: user.id, role: user.role };
  return signJwt(payload, jwtSecret, opts);
};

const verifyToken = token => verifyJwt(token, jwtSecret);

const isAuthenticated = async (req, res, next) => {
  try {
    let token;

    if (req.cookies.sid) {
      token = req.cookies.sid;
    } else if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
      ([, token] = req.headers.authorization.split(' '));
    }

    if (token) {
      const { id, role } = await verifyJwt(token, jwtSecret);
      const model = role === 'admin' ? Admin : User;

      req.user = await model.query().findById(id).throwIfNotFound();
      next();
    } else {
      res.status(401).send(ApiError.unauthorized());
    }
  } catch (e) {
    res.status(401).send(ApiError.unauthorized());
  }
};

const hasRoles = roles => [
  isAuthenticated,
  (req, res, next) => {
    if (roles.includes(req.user.role)) {
      next();
    } else {
      res.status(403).send(ApiError.forbidden());
    }
  },
];
const isAdmin = hasRoles(['admin']);
const isUser = hasRoles(['user']);
const isCourier = hasRoles(['courier']);

// eslint-disable-next-line no-unused-vars
const authBearer = key => async (req, res, next) => {
  try {
    const { authorization } = req.headers;

    if (authorization && authorization.startsWith('Bearer ')) {
      const [, token] = authorization.split(' ');

      if (token) {
        await verifyJwt(token, key);
        return next();
      }
    }

    res.status(401).send(ApiError.unauthorized());
  } catch (e) {
    res.status(401).send(ApiError.unauthorized());
  }
};

module.exports = {
  signToken,
  verifyToken,
  hasRoles,
  isAuthenticated,
  isAdmin,
  isUser,
  isCourier,
};
