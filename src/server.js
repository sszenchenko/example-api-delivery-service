const config = require('config');
const app = require('./app');
const logger = require('./utils/logger');

const port = config.get('app.port');

app.listen(port, () => {
  logger.info(`Server started on port ${port}`);
});
