const BaseModel = require('./BaseModel');
const Product = require('./Product');
const { joi } = require('../utils/validator');

class OrderProduct extends BaseModel {
  static get tableName() {
    return 'order_products';
  }

  static get fullSchema() {
    return joi.object({
      order_id: joi.number().required(),
      product_id: joi.number().required(),
      price: joi.number().required(),
    });
  }

  static get relationMappings() {
    return {
      product: {
        modelClass: Product,
        relation: this.HasOneRelation,
        join: {
          from: `${this.tableName}.product_id`,
          to: `${Product.tableName}.id`,
        },
      },
    };
  }
}

module.exports = OrderProduct;
