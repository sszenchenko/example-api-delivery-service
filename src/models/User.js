const BaseUser = require('./BaseUser');
const { joi } = require('../utils/validator');

class User extends BaseUser {
  static get tableName() {
    return 'users';
  }

  static get timestamps() {
    return true;
  }

  static get patchSchema() {
    return super.patchSchema.keys({ phone_number: joi.string255() });
  }

  get role() {
    return 'user';
  }

  get isUser() {
    return true;
  }
}

module.exports = User;
