const BaseUser = require('./BaseUser');
const { joi } = require('../utils/validator');

class Courier extends BaseUser {
  static get tableName() {
    return 'couriers';
  }

  static get timestamps() {
    return true;
  }

  static get patchSchema() {
    return super.patchSchema.keys({ phone_number: joi.string255() });
  }

  get role() {
    return 'courier';
  }

  get isCourier() {
    return true;
  }
}

module.exports = Courier;
