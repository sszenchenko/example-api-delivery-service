const { RelationExpression } = require('objection');
const QueryBuilder = require('./QueryBuilder');

class SoftDeleteQueryBuilder extends QueryBuilder {
  constructor(modelClass) {
    super(modelClass);

    this.onBuild((builder) => {
      const ctx = builder.context();

      if (!ctx.eager && !ctx.includeDeleted) {
        builder.whereNull(`${modelClass.tableName}.deleted_at`);
      }
    });
  }

  delete() {
    return this.patch({ deleted_at: new Date() });
  }

  eager(exp, filters) {
    this.mergeContext({ eager: exp instanceof RelationExpression });
    return super.eager(exp, filters);
  }
}

module.exports = SoftDeleteQueryBuilder;
