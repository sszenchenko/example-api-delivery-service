const objection = require('objection');

class QueryBuilder extends objection.QueryBuilder {
  patch(modelOrObject) {
    // in case if data is objection.Model validation is omitted
    if (modelOrObject instanceof objection.Model) {
      modelOrObject = { ...modelOrObject };
    }

    return super.patch(modelOrObject);
  }

  orderBy(column, direction = 'asc') {
    if (column.includes(',')) {
      column.split(',').forEach((c) => {
        // eslint-disable-next-line no-shadow
        let direction = 'asc';

        if (c.startsWith('-')) {
          c = c.substr(1);
          direction = 'desc';
        }

        super.orderBy(c, direction);
      });
    } else {
      super.orderBy(column, direction);
    }

    return this;
  }

  async exists(params) {
    const subquery = this.select(1);

    if (params) {
      subquery.where(params);
    }

    const { rows: [{ exists }] } = await this.knex().raw(`select exists (${subquery})`);
    return exists;
  }
}

module.exports = QueryBuilder;
