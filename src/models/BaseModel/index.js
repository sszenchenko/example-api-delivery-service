const shortid = require('shortid');
const { Model, lit } = require('objection');
const { DbErrors } = require('objection-db-errors');
const postgresArray = require('postgres-array');
const moment = require('moment');
const JoiValidator = require('./joiValidator');
const QueryBuilder = require('./QueryBuilder');
const SoftDeleteQueryBuilder = require('./SoftDeleteQueryBuilder');

class BaseModel extends DbErrors(Model) {
  static createValidator() {
    return new JoiValidator();
  }

  static get QueryBuilder() {
    if (this.timestamps) {
      return SoftDeleteQueryBuilder;
    }

    return QueryBuilder;
  }

  static get useLimitInFirst() {
    return true;
  }

  async $beforeInsert(ctx) {
    await super.$beforeInsert(ctx);

    if (this.constructor.uuid) {
      this.id = shortid.generate();
    }

    if (this.constructor.hash) {
      this.hash = shortid.generate();
    }

    if (this.constructor.timestamps) {
      this.created_at = new Date();
    }
  }

  async $beforeUpdate(opts, ctx) {
    await super.$beforeUpdate(opts, ctx);

    if (this.constructor.timestamps) {
      this.updated_at = new Date();
    }
  }

  $formatJson(json, restrictedKeys = []) {
    json = super.$formatJson(json);
    // hash is present in most models
    restrictedKeys.push('hash');

    Object.keys(json).forEach((k) => {
      if (restrictedKeys.includes(k)) {
        delete json[k];
        return;
      }

      if (typeof json[k] === 'string' && /^{.*}$/g.test(json[k])) {
        json[k] = postgresArray.parse(json[k]);
      }

      if (json[k] instanceof Date) {
        json[k] = moment(json[k]).format('YYYY-MM-DD HH:mm:ss');
      }
    });

    return json;
  }

  castToArray(field, arrayType) {
    if (this[field]) {
      this[field] = lit(this[field]).asArray().castTo(`${arrayType}[]`);
    }
  }
}

module.exports = BaseModel;
