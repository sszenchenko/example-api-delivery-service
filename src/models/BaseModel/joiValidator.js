/* eslint-disable class-methods-use-this */

const { Validator } = require('objection');
const { validate } = require('../../utils/validator');

class JoiValidator extends Validator {
  validate(args) {
    const { model, json, options } = args;
    const keys = Object.keys(json);

    if (model.constructor.timestamps && options.patch && keys.includes('deleted_at') && keys.length === 1) {
      return json;
    }

    const schema = options.patch ? model.constructor.patchSchema : model.constructor.fullSchema;
    return validate(json, schema);
  }

  beforeValidate({ model, json, opts }) {
    model.$beforeValidate(null, json, opts);
  }

  afterValidate({ model, json, opts }) {
    model.$afterValidate(null, json, opts);
  }
}

module.exports = JoiValidator;
