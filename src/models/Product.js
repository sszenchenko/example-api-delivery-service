const BaseModel = require('./BaseModel');
const { joi } = require('../utils/validator');

class Product extends BaseModel {
  static get tableName() {
    return 'products';
  }

  static get timestamps() {
    return true;
  }

  static get patchSchema() {
    return joi.object({
      name: joi.string255(),
      price: joi.number().positive(),
    });
  }

  static get fullSchema() {
    return this.patchSchema.requiredKeys('name', 'price');
  }
}

module.exports = Product;
