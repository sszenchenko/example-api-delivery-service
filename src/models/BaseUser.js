const bcrypt = require('bcryptjs');
const BaseModel = require('./BaseModel');
const { joi } = require('../utils/validator');

class BaseUser extends BaseModel {
  static get patchSchema() {
    return joi.object({
      name: joi.string255(),
      login: joi.string255(),
      email: joi.string255(),
      password: joi.string().min(6),
    });
  }

  static get fullSchema() {
    return this.patchSchema.requiredKeys('name', 'login', 'email', 'password');
  }

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext);
    await this.hashPassword();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeUpdate(opt, queryContext);
    await this.hashPassword();
  }

  async hashPassword() {
    if (this.password) {
      this.password = await bcrypt.hash(this.password, 10);
    }
  }
}

module.exports = BaseUser;
