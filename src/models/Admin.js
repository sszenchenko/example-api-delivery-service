const BaseUser = require('./BaseUser');

class Admin extends BaseUser {
  static get tableName() {
    return 'admins';
  }

  get role() {
    return 'admin';
  }

  get isAdmin() {
    return true;
  }
}

module.exports = Admin;
