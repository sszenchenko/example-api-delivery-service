const Admin = require('./Admin');
const User = require('./User');
const Courier = require('./Courier');
const Product = require('./Product');
const Order = require('./Order');
const OrderProduct = require('./OrderProduct');

module.exports = {
  Admin,
  User,
  Courier,
  Product,
  Order,
  OrderProduct,
};
