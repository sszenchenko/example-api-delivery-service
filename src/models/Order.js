const BaseModel = require('./BaseModel');
const OrderProduct = require('./OrderProduct');
const { joi } = require('../utils/validator');

class Order extends BaseModel {
  static get tableName() {
    return 'orders';
  }

  static get timestamps() {
    return true;
  }

  static get patchSchema() {
    return joi.object({
      price: joi.number().positive(),
      deliver_at: joi.date(),
      delivery_address: joi.string(),
      status: joi.string().valid(
        this.STATUS_ACCEPTED,
        this.STATUS_DONE,
        this.STATUS_CANCELLED,
      ),
      payment_method: joi.string().valid('card', 'cash'),
      notes: joi.string255(),
      courier_id: joi.number(),
    });
  }

  static get fullSchema() {
    return this.patchSchema
      .requiredKeys('price', 'deliver_at', 'delivery_address', 'payment_method', 'user_id')
      .keys({
        status: joi.strip(),
        courier_id: joi.strip(),
      });
  }

  static get STATUS_PENDING() {
    return 'pending';
  }

  static get STATUS_ACCEPTED() {
    return 'accepted';
  }

  static get STATUS_DONE() {
    return 'done';
  }

  static get STATUS_CANCELLED() {
    return 'cancelled';
  }

  static get relationMappings() {
    return {
      products: {
        relation: this.HasManyRelation,
        model: OrderProduct,
        join: {
          from: `${this.tableName}.id`,
          to: `${OrderProduct}.order_id`,
        },
      },
    };
  }
}

module.exports = Order;
