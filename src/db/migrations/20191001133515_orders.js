exports.up = knex => knex.raw(`
  CREATE TYPE payment_method AS ENUM ('card', 'cash');
  
  CREATE TYPE order_status AS ENUM ('pending', 'accepted', 'done', 'cancelled');
  
  CREATE TABLE orders (
    id serial PRIMARY KEY,
    price numeric(12,2) NOT NULL CHECK (price > 0),
    deliver_at timestamptz NOT NULL,
    delivery_address text NOT NULL,
    status order_status NOT NULL DEFAULT 'pending',
    payment_method payment_method NOT NULL,
    notes varchar(255),
    user_id int NOT NULL REFERENCES users,
    courier_id int REFERENCES couriers,
    created_at timestamptz DEFAULT now(),
    updated_at timestamptz,
    deleted_at timestamptz
  );
`);

exports.down = knex => knex.raw(`
  DROP TABLE IF EXISTS orders;
`);
