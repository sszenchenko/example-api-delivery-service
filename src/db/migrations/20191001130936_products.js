exports.up = knex => knex.raw(`
  CREATE TABLE products (
    id serial PRIMARY KEY,
    name varchar(255) NOT NULL,
    price numeric(12,2) NOT NULL CHECK (price > 0),
    created_at timestamptz DEFAULT now(),
    updated_at timestamptz,
    deleted_at timestamptz
  );
`);

exports.down = knex => knex.raw(`
  DROP TABLE IF EXISTS products;
`);
