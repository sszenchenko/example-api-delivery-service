exports.up = knex => knex.raw(`
  CREATE TABLE IF NOT EXISTS admins (
    id serial PRIMARY KEY,
    name varchar(255) NOT NULL,
    login varchar(255) UNIQUE NOT NULL,
    email varchar(255) UNIQUE NOT NULL,
    password text NOT NULL
  );
`);

exports.down = knex => knex.raw(`
  DROP TABLE IF EXISTS admins;
`);
