exports.up = knex => knex.raw(`
  CREATE TABLE order_products (
    order_id int NOT NULL REFERENCES orders,
    product_id int NOT NULL REFERENCES products,
    price numeric(12,2) NOT NULL CHECK (price > 0),
    PRIMARY KEY (order_id, product_id)
  );
`);

exports.down = knex => knex.raw(`
  DROP TABLE IF EXISTS order_products;
`);
