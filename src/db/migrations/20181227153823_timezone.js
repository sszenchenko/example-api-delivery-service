exports.up = knex => knex.raw(`
  ALTER DATABASE ${knex.client.config.connection.database} SET timezone = 'Europe/Kiev';
`);

exports.down = (knex, Promise) => Promise.resolve();
