exports.up = knex => knex.raw(`
  CREATE TABLE IF NOT EXISTS couriers (
    id serial PRIMARY KEY,
    name varchar(255) NOT NULL,
    phone_number varchar(255) NOT NULL,
    login varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password text NOT NULL,
    created_at timestamptz DEFAULT now(),
    updated_at timestamptz,
    deleted_at timestamptz
  );
  CREATE UNIQUE INDEX couriers_login_idx ON couriers (login) WHERE deleted_at IS NULL;
  CREATE UNIQUE INDEX couriers_email_idx ON couriers (email) WHERE deleted_at IS NULL;
`);

exports.down = knex => knex.raw(`
  DROP TABLE IF EXISTS couriers;
`);
