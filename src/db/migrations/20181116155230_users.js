exports.up = knex => knex.raw(`
  CREATE TABLE IF NOT EXISTS users (
    id serial PRIMARY KEY,
    name varchar(255) NOT NULL,
    phone_number varchar(255) NOT NULL,
    login varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password text NOT NULL,
    created_at timestamptz DEFAULT now(),
    updated_at timestamptz,
    deleted_at timestamptz
  );
  CREATE UNIQUE INDEX users_login_idx ON users (login) WHERE deleted_at IS NULL;
  CREATE UNIQUE INDEX users_email_idx ON users (email) WHERE deleted_at IS NULL;
`);

exports.down = knex => knex.raw(`
  DROP TABLE IF EXISTS users;
`);
