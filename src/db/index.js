/* eslint-disable no-underscore-dangle */
const config = require('config');
const pg = require('pg');
const knex = require('knex');
const moment = require('moment');
const { Model } = require('objection');
const logger = require('../utils/logger');

const db = knex({
  client: 'pg',
  connection: config.get('db'),
});
const queriesStartTime = {};

Model.knex(db);
pg.types.setTypeParser(20, parseInt);

db
  .on('query', (query) => {
    if (logQuery(query)) {
      queriesStartTime[query.__knexQueryUid] = Date.now();
    }
  })
  .on('query-response', (res, query) => {
    if (logQuery(query)) {
      const time = Date.now() - queriesStartTime[query.__knexQueryUid];
      delete queriesStartTime[query.__knexQueryUid];
      logger.info(`${formatQuery(query)} - ${time} ms`);
    }
  });

db.on('query-error', (error, data) => {
  logger.error(`query error: ${formatQuery(data)}\n${error.stack}`);
});

function logQuery(query) {
  if (query.options) {
    const { log = true } = query.options;
    return log;
  }

  return true;
}

function formatQuery(data) {
  // eslint-disable-next-line prefer-const
  let { sql, bindings } = data;

  bindings.forEach((b, i) => {
    if (b instanceof Date) {
      b = moment(b).format();
    }

    if (typeof b === 'string') {
      b = `'${b}'`;
    }

    sql = sql.replace(new RegExp(`\\$${i + 1}`), b);
  });

  return sql;
}

module.exports = db;
