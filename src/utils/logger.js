const winston = require('winston');

winston.configure({
  transports: [
    new (winston.transports.Console)({
      level: process.env.NODE_ENV === 'test' ? 'error' : 'info',
      handleExceptions: true,
      humanReadableUnhandledException: true,
      timestamp: () => new Date().toISOString(),
      formatter: (options) => {
        let meta = '';

        if (options.meta && Object.keys(options.meta).length) {
          if (options.meta.stack) {
            options.message = Array.isArray(options.meta.stack)
              ? options.meta.stack.map(e => e).join('\n')
              : options.meta.stack;
          } else {
            meta = JSON.stringify(options.meta, null, 2);
          }
        }

        const msg = `${options.timestamp()} ${options.level.toUpperCase()}: ${options.message} ${meta}`;
        return winston.config.colorize(options.level, msg);
      },
    }),
  ],
  exitOnError: false,
});

winston.stream = {
  write(message) {
    winston.info(message);
  },
};

module.exports = winston;
