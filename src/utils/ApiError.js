const httpStatus = require('http-status');

class ApiError {
  constructor(message, statusCode, details) {
    this.statusCode = statusCode;
    this.error = httpStatus[statusCode];
    this.message = message || this.error;
    this.details = details;
  }

  static unauthorized(message) {
    return new ApiError(message, 401);
  }

  static forbidden() {
    return new ApiError(null, 403);
  }

  static badRequest(message, details) {
    return new ApiError(message, 400, details);
  }

  static paymentFailed(message) {
    return new ApiError(message, 402);
  }

  static notFound(message) {
    return new ApiError(message, 404);
  }

  static conflict(message) {
    return new ApiError(message, 409);
  }

  static unsupportedMediaType(message) {
    return new ApiError(message, 415);
  }

  static internal() {
    return new ApiError(null, 500);
  }
}

module.exports = ApiError;
