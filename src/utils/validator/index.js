/* eslint-disable no-shadow */
const joi = require('joi');
const { ValidationError } = require('objection');

const rules = [
  {
    name: 'required',
    // eslint-disable-next-line no-unused-vars
    setup(params) {
      return this.not(null).exist();
    },
  },
];
const extendType = type => joi => ({ name: type, base: joi[type]().allow(null), rules });
const customJoi = joi
  .extend(joi => ({
    name: 'string',
    base: joi.string().allow('', null).trim(),
    rules: [
      {
        name: 'required',
        // eslint-disable-next-line no-unused-vars
        setup(params) {
          return this.not('', null).exist();
        },
      },
      {
        name: 'dateFormat',
        // eslint-disable-next-line no-unused-vars
        setup(params) {
          return this.regex(/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/, 'date format');
        },
      },
    ],
    // eslint-disable-next-line no-unused-vars
    coerce(value, state, prefs) {
      if (value) {
        return String(value);
      }

      return value;
    },
  }))
  .extend(joi => ({
    name: 'string255',
    base: joi.string().max(255),
  }))
  .extend(joi => ({
    name: 'email',
    base: joi.string255().email(),
  }))
  .extend(extendType('array'))
  .extend(extendType('boolean'))
  .extend(extendType('bool'))
  .extend(extendType('date'))
  .extend(extendType('number'))
  .extend(extendType('object'))
  .defaults(schema => schema.options({
    stripUnknown: true,
    abortEarly: false,
  }));

function validate(value, schema, options) {
  const res = customJoi.validate(value, schema, options);

  if (res.error) {
    const data = res.error.details.map(e => ({ message: e.message, path: e.path[0] }));
    throw new ValidationError({ data });
  }

  return res.value;
}

module.exports = { joi: customJoi, validate };
