const joi = require('joi');
const { joi: customJoi } = require('./index');

const fetchSchema = joi.object({
  fields: joi.array().items(joi.string()),
  filter: joi.object().default({}),
  page: joi.object({
    number: joi.number().default(0),
    size: joi.number().default(50),
  }).default({
    number: 0,
    size: 50,
  }),
  sort: joi.string(),
});

const loginSchema = {
  body: customJoi.object({
    email: customJoi.email().required(),
    password: customJoi.string().min(6).required(),
  }),
};

module.exports = { fetchSchema, loginSchema };
