/* eslint-disable object-property-newline */
const generate = require('nanoid/async/generate');
const moment = require('moment');

const startOfWeek = date => moment(date).startOf('week').toDate();

const startOfMonth = date => moment(date).startOf('month').toDate();

const calcPercent = (value, percent, precision = 2) => Number(((value / 100) * percent).toFixed(precision));

const generateID = () => generate('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 24);

const random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

const decodeCP1251 = (str) => {
  // eslint-disable-next-line max-len
  const cp1251 = 'ЂЃ‚ѓ„…†‡€‰Љ‹ЊЌЋЏђ‘’“”•–—�™љ›њќћџ ЎўЈ¤Ґ¦§Ё©Є«¬­®Ї°±Ііґµ¶·ё№є»јЅѕїАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя';

  return str.replace(/%(..)/g, (s, p) => {
    p = parseInt(p, 16);
    return p < 128 ? String.fromCharCode(p) : cp1251[p - 128];
  });
};

module.exports = {
  startOfWeek,
  startOfMonth,
  calcPercent,
  generateID,
  random,
  decodeCP1251,
};
