const express = require('express');
const ProductController = require('../../controllers/ProductController');
const { auth, validate } = require('../../middlewares');
const { joi } = require('../../utils/validator');
const { fetchSchema } = require('../../utils/validator/schemas');

const router = express.Router();
const getSchema = {
  query: fetchSchema.keys({
    filter: {
      name: joi.string(),
      min_price: joi.number(),
      max_price: joi.number().greater(joi.ref('min_price')),
    },
  }),
};

router.get('/', validate(getSchema), ProductController.getAll());

router.use('/:id', ProductController.checkEntityExists());

router.get('/:id', ProductController.getById());

router.use(auth.isAdmin);

router.post('/', ProductController.create());

router.put('/:id', ProductController.update());

router.delete('/:id', ProductController.delete());

module.exports = router;
