const express = require('express');
const AuthController = require('../../controllers/AuthController');
const UserController = require('../../controllers/UserController');
const AdminController = require('../../controllers/AdminController');
const CourierController = require('../../controllers/CourierController');
const { auth, validate } = require('../../middlewares');
const { joi } = require('../../utils/validator');

const router = express.Router();
const loginSchema = {
  body: {
    username: joi.string().required(),
    password: joi.string().min(6).required(),
  },
};

router.get('/', auth.isAuthenticated, AuthController.getProfile());

router.post('/login', validate(loginSchema), UserController.login());

router.post('/admin/login', validate(loginSchema), AdminController.login());

router.post('/courier/login', validate(loginSchema), CourierController.login());

router.post('/signup', UserController.create());

router.post('/logout', auth.isAuthenticated, AuthController.logout());

module.exports = router;
