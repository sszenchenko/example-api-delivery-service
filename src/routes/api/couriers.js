const express = require('express');
const { auth } = require('../../middlewares');
const CourierController = require('../../controllers/CourierController');

const router = express.Router();

router.use(auth.isAdmin);

router.get('/', CourierController.getAll());

router.get('/list', CourierController.list());

router.post('/', CourierController.create());

router.use('/:id', CourierController.checkEntityExists());

router.put('/:id', CourierController.update());

router.delete('/:id', CourierController.delete());

module.exports = router;
