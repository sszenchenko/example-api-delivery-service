const express = require('express');

const router = express.Router();

router.use('/auth', require('./auth'));
router.use('/users', require('./users'));
router.use('/couriers', require('./couriers'));
router.use('/products', require('./products'));

module.exports = router;
