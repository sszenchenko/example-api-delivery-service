const express = require('express');
const UserController = require('../../controllers/UserController');
const { auth, validate } = require('../../middlewares');
const { joi } = require('../../utils/validator');
const { fetchSchema } = require('../../utils/validator/schemas');

const router = express.Router();
const getSchema = {
  query: fetchSchema.keys({
    filter: {
      search: joi.string(),
    },
  }),
};

router.get('/list', auth.isAuthenticated, UserController.list());

router.use(auth.isAdmin);

router.get('/', validate(getSchema), UserController.getAll());

router.get('/:id', UserController.checkEntityExists(), UserController.getById());

router.post('/', UserController.create());

router.post('/:id/login', UserController.loginByAdmin());

router.put('/:id', UserController.checkEntityExists(), UserController.update());

router.delete('/:id', UserController.checkEntityExists(), UserController.delete());

module.exports = router;
