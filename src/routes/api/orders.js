const express = require('express');
const { auth, validate } = require('../../middlewares');
const { joi } = require('../../utils/validator');
const { Order } = require('../../models');
const OrderController = require('../../controllers/OrderController');

const router = express.Router();
const createSchema = {
  body: Order.fullSchema.keys({
    price: joi.strip(),
    product_ids: joi.array().items(joi.number()).min(1).required(),
  }),
};
const updateSchema = {
  body: joi.object({
    status: joi.string().valid(Order.STATUS_ACCEPTED, Order.STATUS_DONE),
  }),
};

router.get('/', auth.isAuthenticated, OrderController.getAll());

router.post('/', auth.isUser, validate(createSchema), OrderController.create());

// eslint-disable-next-line max-len
router.put('/:id', auth.isCourier, OrderController.checkEntityExists(), validate(updateSchema), OrderController.update());

module.exports = router;
